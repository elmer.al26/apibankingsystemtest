package org.bankingApi;

/**
 * Handle the account
 */
public class Account {

    static final double MINIMUM_BALANCE = 100.00;
    static final double TOP_PERCENTAGE_WITHDRAW = 90.00;

    protected int accountId;
    protected String accountName;
    protected User owner;
    protected double balance;

    public Account(int accountId, String accountName, User owner){
        this.accountName = accountName;
        this.setOwner(owner);
        this.accountId = accountId;
        setBalance(MINIMUM_BALANCE);
    }

    public int getAccountId(){
        return accountId;
    }

    public User getOwner() {
        return owner;
    }

    private void setOwner(User owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountName(){
        return accountName;
    }

    public void deposit(double amount){
        balance = balance + amount;
    }

    public void withdraw(double amount){
        if(isAbleToWithDraw(amount)){
            balance = balance - amount;
        }
    }

    private boolean isAbleToWithDraw(double amount) {
        if(!minimumBalanceCondition(amount)){
            System.out.println(" Not able to Withdraw, minimum balance should be 100");
            return false;
        } else if (withDrawMoreThan90Percent(amount)) {
            System.out.println(" Not able to Withdraw 90% of total balance");
            return false;
        } else if (depositMoreThan10k(amount)) {
            System.out.println(" Not able to Deposit more than 10,000 in a single transaction");
            return false;
        }
        return true;
    }

    /**
     * @param amount to withdraw
     * @return true if meets the minimum condition, else false
     */
    private boolean minimumBalanceCondition(double amount) {
        return (balance - amount) >= MINIMUM_BALANCE;
    }

    private boolean withDrawMoreThan90Percent(double amount){
        return (amount/balance*100) > TOP_PERCENTAGE_WITHDRAW;
    }

    private boolean depositMoreThan10k(double amount){
        if(amount > 10000.00){return true;}
        return false;
    }
}
