package org.bankingApi;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static int userId =0;
    public static int accountId =0;
    private static HashMap<Integer, User> listUser;
    public static void main(String[] args) {
        listUser = new HashMap<>();
        showMainMenu();
    }
    public static void showMainMenu(){
        boolean exitOption = false;
        Scanner scanner = new Scanner(System.in);
        while (!exitOption){
            System.out.println( "*********** Menu ********");
            System.out.println( "1.- List users ");
            System.out.println( "2.- Create user ");
            System.out.println( "0.- Exit Option ");
            System.out.println( "*************************");

            int option = scanner.nextInt();
            switch (option){
                case 0:
                    exitOption = true;
                    System.out.println("System Exit!!!");
                    break;
                case 1:
                    showUsers();
                    break;
                case 2:
                    createUser();
                    break;
                default:
                    System.out.println("Choose a valid option");
            }
        }
    }
    public static void showUsers(){
        if(listUser.isEmpty()){
            System.out.println(" There is no Users!!!");
            return;
        }

        boolean userExitOption = false;
        while (!userExitOption){
            System.out.println(" **** LIST OF USERS ****");
            System.out.println("Id - User Name");
            for (User user: listUser.values()) {
                System.out.println(user.getUserId() + ".- "+ user.getFirstName() +" - "+user.getLastName());
            }
            System.out.println(" *******************");
            System.out.println(" *** Choose a user **");

            User choosenUser = choiceUser();
            if(choosenUser != null){
                accountOption(choosenUser);
            }else{
                userExitOption = true;
            }
        }
    }

    public static User choiceUser(){
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Choose User Id");
        int option = scanner.nextInt();
        if(listUser.containsKey(option))
        {
            return listUser.get(option);
        }
        System.out.println("No valid option chose");
        return null;
    }

    public static void createUser(){
        Scanner scanner = new Scanner(System.in);
        userId+=1;
        System.out.println("Set the first Name..");
        String firstname = scanner.next();
        System.out.println("Set the last Name..");
        String lastname = scanner.next();

        User user01= new User(userId, firstname, lastname);
        listUser.put(userId, user01);

    }

    public static void accountOption(User user){
        boolean accountExitOption=false;
        while (!accountExitOption){
            System.out.println( "*********** Account Menu ********");
            System.out.println( "1.- List Accounts ");
            System.out.println( "2.- Create Account ");
            System.out.println( "0.- Exit Option ");
            System.out.println( "*************************");

            Scanner scanner = new Scanner(System.in);
            int option = scanner.nextInt();
            switch (option){
                case 0:
                    accountExitOption = true;
                    System.out.println("Account option Exit!!!");
                    break;
                case 1:
                    showAccounts(user);
                    break;
                case 2:
                    createAccount(user);
                    break;
                default:
                    System.out.println("Choose a valid option");
            }
        }
    }

    public static void showAccounts(User user){
        if(user.getListOfAccount().isEmpty()){
            System.out.println(" There is no Accounts");
        }else {
            System.out.println(" **** LIST OF ACCOUNTS ****");
            user.showAccounts();
            Account accountChosen = chooseAccount(user);
            if(accountChosen != null){
                showAccountActionOption(accountChosen);
            }
        }
    }

    private static void showAccountActionOption(Account accountChosen) {
        boolean accountExitOption=false;
        while (!accountExitOption){
            System.out.println( "*********** Account Options ********");
            System.out.println( "1.- Deposit ");
            System.out.println( "2.- Withdraw ");
            System.out.println( "3.- Delete ");
            System.out.println( "0.- Exit Option ");
            System.out.println( "*************************");

            Scanner scanner = new Scanner(System.in);
            int option = scanner.nextInt();
            switch (option){
                case 0:
                    accountExitOption = true;
                    System.out.println("Account option Exit!!!");
                    break;
                case 1:
                    System.out.println("Enter amount to Deposit");
                    double amountToDeposit = scanner.nextDouble();
                    depositToAccount(accountChosen, amountToDeposit);
                    break;
                case 2:
                    System.out.println("Enter amount to Withdraw");
                    double amountToWithdraw = scanner.nextDouble();
                    withdrawToAccount(accountChosen, amountToWithdraw);
                    break;
                case 3:
                    deleteAccount(accountChosen);
                    accountExitOption = true;
                default:
                    System.out.println("Choose a valid option");
            }
        }
    }

    private static void deleteAccount(Account accountChosen) {
        accountChosen.getOwner().removeAccount(accountChosen.getAccountId());
    }

    public static Account chooseAccount(User user){
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Choose Account Id");
        int option = scanner.nextInt();
        if(user.getListOfAccount().containsKey(option))
        {
            return user.getListOfAccount().get(option);
        }
        System.out.println("No valid option chose");
        return null;
    }

    public static void createAccount(User currentuser){
        accountId+=1;
        System.out.println("Set account name");
        Scanner scanner = new Scanner(System.in);
        String accountName = scanner.next();
        Account bnbAccount = new Account(accountId, accountName, currentuser);
        currentuser.addAccount(bnbAccount);
    }

    public static void depositToAccount(Account account, double amount){
        account.deposit(amount);
    }

    public static void withdrawToAccount(Account account, double amount){
        account.withdraw(amount);
    }
}