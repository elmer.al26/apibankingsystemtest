package org.bankingApi;

import java.util.HashMap;

public class User {
    private final int userId;
    private final String firstName;
    private final String lastName;
    private HashMap<Integer, Account> listOfAccount;

    public User(int userId, String name, String lastName){
        this.userId = userId;
        this.firstName = name;
        this.lastName = lastName;
        listOfAccount = new HashMap<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getUserId() {
        return userId;
    }


    public HashMap<Integer, Account> getListOfAccount() {
        return listOfAccount;
    }

    public void showAccounts(){
        if(listOfAccount.isEmpty()){
            System.out.println("There is no Accounts!!!");
        }else {
            System.out.println("Id - Name - Balance");
            for (Account account: listOfAccount.values()){
                System.out.println(account.getAccountId()+ " "+ account.getAccountName() +" "+ account.getBalance());
            }
        }
    }

    public void addAccount(Account account) {
        this.listOfAccount.put(account.getAccountId(), account);
    }

    public void removeAccount(int accountId){
        this.listOfAccount.remove(accountId);
    }
}
